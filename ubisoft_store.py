import logging
from core import util, handlers

log = logging.getLogger('Cecily.UbisoftStore')


class UbisoftHandler(handlers.EventHandler):

    def handle(self, event):
        super().handle(event)
        # TODO

def get_free_games():
    log.info('Querying the ubisoft store for free games')
    try:
        src = util.read_rendered_webpage_source('https://free.ubisoft.com/', 5)
    except RuntimeError:
        log.error('Error opening the ubisoft store')
        return None

    games = []

    while src.find('<span>Game giveaway</span>') != -1:
        start = src.find('<span>Game giveaway</span>')
        title_start = src.find('href="https://register.ubisoft.com/', start) + 35
        title_end = src.find("\"", title_start)
        title = slice_and_capitalize(src[title_start:title_end])
        games.append(title)
        src = src[title_end:]

    return games


def slice_and_capitalize(title):
    """ Make titles from link texts ie
        might_and_magic_chess_royale -> Might And Magic Chess Royale
                      rabbids-coding -> Rabbids Coding
     """
    import string
    return string.capwords(title.replace('_', ' ').replace('-', ' '))

