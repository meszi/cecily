import logging
from core.chat import Chat
from core.util import read_webpage_source, extract_number

log = logging.getLogger('Cecily.Virus')

URL = 'https://koronavirus.gov.hu/'
END_TAG = '</div>'


def trim_spaces(str):
    return str.replace(' ', '')


def parse_stats():
    log.info('Trying to get information about the virus')
    try:
        src = read_webpage_source(URL)
    except RuntimeError:
        log.error('Error opening %s', URL)
        return None

    tag = 'api-fertozott-pest">'
    start_hun = src.find(tag)
    end_hun = src.find(END_TAG, start_hun)
    pest_num = int(trim_spaces(src[start_hun+len(tag):end_hun]))

    tag = 'api-fertozott-videk">'
    start_hun = src.find(tag)
    end_hun = src.find(END_TAG, start_hun)
    videk_num = int(trim_spaces(src[start_hun+len(tag):end_hun]))
    infected_hun = pest_num + videk_num

    tag = 'api-gyogyult-pest">'
    cured_hun_start = src.find(tag)
    cured_hun_end = src.find(END_TAG, cured_hun_start)
    pest_num = int(trim_spaces(src[cured_hun_start+len(tag):cured_hun_end]))

    tag = 'api-gyogyult-videk">'
    cured_hun_start = src.find(tag)
    cured_hun_end = src.find(END_TAG, cured_hun_start)
    videk_num = int(trim_spaces(src[cured_hun_start+len(tag):cured_hun_end]))
    cured_hun = pest_num + videk_num

    tag = 'api-elhunyt-pest">'
    dead_hun_start = src.find(tag)
    dead_hun_end = src.find(END_TAG, dead_hun_start)
    pest_num = int(trim_spaces(src[dead_hun_start+len(tag):dead_hun_end]))

    tag = 'api-elhunyt-videk">'
    dead_hun_start = src.find(tag)
    dead_hun_end = src.find(END_TAG, dead_hun_start)
    videk_num = int(trim_spaces(src[dead_hun_start+len(tag):dead_hun_end]))
    dead_hun = pest_num + videk_num

    tag = 'api-karantenban">'
    quarantined_hun_start = src.find(tag)
    quarantined_hun_end = src.find(END_TAG, quarantined_hun_start)
    quarantined_hun = trim_spaces(src[quarantined_hun_start+len(tag):quarantined_hun_end])

    tag = 'api-mintavetel">'
    tested_hun_start = src.find(tag)
    tested_hun_end = src.find(END_TAG, tested_hun_start)
    tested_hun = trim_spaces(src[tested_hun_start+len(tag):tested_hun_end])

    msg = Chat.with_format('\nHungary\n', 'b')
    msg += 'Infected: {}, Cured: {}, Dead: {}, Quarantined: {}, Tested: {}'.format(infected_hun, cured_hun, dead_hun, quarantined_hun, tested_hun)

    tag = 'api-fertozott-global">'
    infected_global_start = src.find(tag)
    infected_global_end = src.find(END_TAG, infected_global_start)
    infected_global = trim_spaces(src[infected_global_start+len(tag):infected_global_end])

    tag = 'api-gyogyult-global">'
    cured_global_start = src.find(tag)
    cured_global_end = src.find(END_TAG, cured_global_start)
    cured_global = trim_spaces(src[cured_global_start+len(tag):cured_global_end])

    tag = 'api-elhunyt-global">'
    dead_global_start = src.find(tag)
    dead_global_end = src.find(END_TAG, dead_global_start)
    dead_global = trim_spaces(src[dead_global_start+len(tag):dead_global_end])

    msg += Chat.with_format('\nWorld\n', 'b')
    msg += 'Infected: {}, Cured: {}, Dead: {}'.format(infected_global, cured_global, dead_global)

    '<p>Legutolsó frissítés dátuma:' '</p>'
    time_start = src.find('<p>Legutolsó frissítés dátuma: ')
    time_end = src.find('</p>', time_start)
    time = src[time_start+30:time_end]
    # add spaces around : to avoid :0 emoji
    time = time.replace(':', ' : ')
    msg += '\nAs of {}'.format(time)

    return msg

