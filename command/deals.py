from core.chat import Chat
from core.util import read_webpage_source

URL = 'https://isthereanydeal.com/search/?q='


def get_deal_for_game(search_term):
    # flatten command parameter list, since game name can contain spaces
    search_term = '+'.join(search_term)
    try:
        src = read_webpage_source(URL + search_term)
    except RuntimeError:
        return None
    if 'There were no games found for' in src:
        return None

    start = src.find('<div class=\'card-container\'>')
    end = src.find('</div></div></div>', start)
    src = src[start:end]
    title_start = src.find('card__title')
    title = src[src.find('>', title_start)+1 : src.find('<', title_start)]
    link = src[src.find('href=\'', title_start)+6 : src.find('\'>', title_start)]
    link = 'https://isthereanydeal.com' + link + ' '
    price_start = src.find('Current Best')
    price_start = src.find('numtag__primary', price_start)
    price = src[src.find('>', price_start)+1 : src.find('<', price_start)].replace('&euro;', ' EUR')
    discount = 'no deals'
    if 'numtag__second' in src:
        discount_start = src.find('numtag__second')
        discount = '-' + src[src.find('>', discount_start)+1 : src.find('<', discount_start)]
    return Chat.with_format('{} : {} ({})'.format(title, price, discount), 'b') + '\n' + link

