# -*- coding: cp1252 -*-

import random
import sys
import logging
from storage import db_agent as DBA
from core.chat import Chat
from core import util, vocabulary


class Command:
    def __init__(self, cmd, invoker, invoker_id, target_mode):
        # if command is parametrized
        cmdstring = cmd.split('\s')

        self.cmd = cmdstring[0]
        self.params = cmdstring[1:]
        self.invoker = invoker
        self.invoker_id = invoker_id
        self.target_mode = target_mode

    def is_private_msg(self):
        return self.target_mode == Command.PRIVATE_MSG

    PRIVATE_MSG = 1
    CHANNEL_MSG = 2


def invoke(command):
    """ Invokes specified command, passing info
    :param command: info tuple, containing cmd string, invoker specs
    """
    COMMANDS[command.cmd](command)


##########################################################################################################
#                                   ACTUAL COMMANDS START HERE
##########################################################################################################
log = logging.getLogger('Cecily.commands')


def ping(cmd):
    """ Cecily replies if she's available """
    Chat.send_to_target(random.choice(vocabulary.ping), cmd.invoker_id, cmd.target_mode)


def help_(cmd):
    """ Displays available commands - always in private message """
    msg = 'Available commands:\\n'
    msg += Chat.with_format('!help', 'b') + ' - Display this message.\\n'
    msg += Chat.with_format('!ping', 'b') + ' - Ping me to see if I\'m around.\\n'
    #msg += Chat.with_format('!convert', 'b') + ' - Convert currencies.\\n'
    #msg += '\\tUsage: <amount> <currency> to <other currency> e. g. !convert 10 eur to huf.\\n'
    # msg += Chat.with_format('!weather', 'b') + ' - Current weather.\\n'
    # msg += '\\tOptional <city> parameter e.g. !weather Paris. (defaults to Budapest).\\n'
    msg += Chat.with_format('!roll', 'b') + ' - Roll a 100 sided die.\\n'
    msg += Chat.with_format('!choose', 'b') + ' - Randomly choose from options. (separate single word options with space)\\n'
    msg += Chat.with_format('!joke', 'b') + ' - I\'ll try and find a joke for you.\\n'
    msg += Chat.with_format('!restart', 'b') + ' - Makes me reconnect.\\n'
    msg += Chat.with_format('!activity', 'b') + ' - Display most recent channel activity.\\n'
    msg += Chat.with_format('!deals', 'b') + ' - Give me a game to search for the best deals.\\n'
    msg += Chat.with_format('!virus', 'b') + ' - See latest stats about COVID-19.\\n'
    Chat.send_to_client(msg, cmd.invoker_id)


def roll(cmd):
    """ Rolls a one-hundred sided die """
    result = cmd.invoker + ' rolled ' + str(random.randint(1, 100))
    log.info(result)
    Chat.send_to_target(Chat.with_format(result, 'b'), cmd.invoker_id, cmd.target_mode)


def choose(cmd):
    """ Chooses randomly from given options """
    if len(cmd.params) < 2:
        Chat.send_to_target('There is not much to choose from here, I\'m afraid...', cmd.invoker_id, cmd.target_mode)
    else:
        chosen = random.choice(cmd.params)
        Chat.send_to_target(random.choice(vocabulary.choice).format(chosen), cmd.invoker_id, cmd.target_mode)


def joke(cmd):
    """ Tells a joke """
    try:
        from core.cecily import config
        with open(config['jokes_file'], encoding='utf-8') as file:
            jokes = file.read().split('\n\n')
            Chat.send_to_target(random.choice(jokes), cmd.invoker_id, cmd.target_mode)
    except (OSError, IOError):
        log.error('Cannot open jokes file')
        Chat.send_to_target('Oh-oh. I couldn\'t find any jokes. Where did I put them? :/', cmd.invoker_id, cmd.target_mode)


def weather(cmd):
    """ Current weather """
    from command import weather
    response = weather.current_weather(cmd)
    if response is not None:
        Chat.send_to_target(Chat.with_format(response, 'b'), cmd.invoker_id, cmd.target_mode)
    else:
        Chat.send_to_target('Hm. I can\'t find that right now.', cmd.invoker_id, cmd.target_mode)


def convert(cmd):
    """ Converts currencies """
    from command import converter
    if len(cmd.params) == 4 and util.is_int_or_float(cmd.params[0]) and (cmd.params[2] == 'in' or cmd.params[2] == 'to'):
        try:
            result = converter.convert(cmd.params[0], cmd.params[1], cmd.params[3])
            if len(result.strip()) == 0:
                Chat.send_to_target('I... I don\'t know, sorry.', cmd.invoker_id, cmd.target_mode)
            else:
                Chat.send_to_target(Chat.with_format(result, 'b'), cmd.invoker_id, cmd.target_mode)
        except RuntimeError:
            log.error('Converting failed')
            Chat.send_to_target('Oh-oh. Sorry, I can\'t seem to do this right now.', cmd.invoker_id, cmd.target_mode)
    else:
        Chat.send_to_target('I don\'t quite get what you mean.', cmd.invoker_id, cmd.target_mode)


def restart(cmd):
    """ Makes Cecily disconnect and stops the process - letting monit restart it. """
    Chat.send_to_channel(random.choice(vocabulary.selfdisconnect))
    Chat.send_to_channel(Chat.with_format('*Cecily disconnects*', 'b+color=red'))
    log.info('exiting...')
    sys.exit(0)


def activity(cmd):
    """ Displays channel activity history """
    response = random.choice(vocabulary.activity_response)
    Chat.send_to_target(response, cmd.invoker_id, cmd.target_mode)
    history = DBA.activity_history(5)
    Chat.send_to_target(Chat.with_format(history, 'b'), cmd.invoker_id, cmd.target_mode)


def deals(cmd):
    """ Gets current best deal on a game from isthereanydeal.com """
    from command import deals
    response = deals.get_deal_for_game(cmd.params)
    if response is None:
        Chat.send_to_target(random.choice(vocabulary.game_not_found_response), cmd.invoker_id, cmd.target_mode)
    else:
        Chat.send_to_target(response, cmd.invoker_id, cmd.target_mode)
    # TODO crawl the grey market as well (eneba.com, https://www.g2a.com/)


def virus(cmd):
    from command import virus
    response = virus.parse_stats()
    if response is None:
        Chat.send_to_target('Uhm... The website doesn\'t seem to respond...', cmd.invoker_id, cmd.target_mode)
    Chat.send_to_target(response, cmd.invoker_id, cmd.target_mode)


def test(cmd):
    from channel_manager import get_description
    get_description()

COMMANDS = {'!ping': ping,
            '!help': help_,
            '!roll': roll,
            '!choose': choose,
            '!joke': joke,
            '!weather': weather,
            #'!convert': convert,
            '!restart': restart,
            '!activity': activity,
            '!deals': deals,
            '!virus': virus,
            '!test': test}




