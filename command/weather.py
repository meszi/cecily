import logging
import json
from core.util import read_webpage_source

log = logging.getLogger('Cecily.weather')

ENDPOINT = 'http://api.openweathermap.org/data/2.5/'
API_KEY = '&APPID=ecb8b0605ce48c2bd6bf92cb2967444b'
UNITS = '&units=metric'

WEATHER_QUERY = 'weather?q='
weather = None

def call(request):
    return read_webpage_source(ENDPOINT+request+API_KEY+UNITS)


def current_weather(cmd):
    # if without parameter, default to Budapest
    city = cmd.params[0] if len(cmd.params) > 0 else 'Budapest, hu'
    log.info('Looking up weather in %s...', city)
    try:
        response = call(WEATHER_QUERY + city)
    except RuntimeError:
        return None

    # TODO caching to file - only allow new requests after 20 min
    # TODO vocabulary based on weather

    global weather
    weather = json.loads(response)
    for entry in weather:
        log.debug('%s -> %s', entry, weather[entry])

    # coord -> {'lon': 19.04, 'lat': 47.5}
    # weather -> [{'id': 803, 'main': 'Clouds', 'description': 'broken clouds', 'icon': '04n'}]
    # base -> stations
    # main -> {'temp': 2.48, 'feels_like': -1.28, 'temp_min': 0, 'temp_max': 4.44, 'pressure': 1018, 'humidity': 52}
    # visibility -> 10000
    # wind -> {'speed': 1.44, 'deg': 218}
    # clouds -> {'all': 67}
    # dt -> 1585773053
    # sys -> {'type': 1, 'id': 6663, 'country': 'HU', 'sunrise': 1585714914, 'sunset': 1585761180}
    # timezone -> 7200
    # id -> 3054643
    # name -> Budapest
    # cod -> 200

    temperature = read_value('main', 'temp')
    wind_speed = read_value('wind', 'speed')
    wind_direction = read_value('wind', 'deg')
    if len(weather['weather']) == 1:
        condition = conditions[weather['weather'][0]['id']]
    else:
        # TODO
        condition = None



    return weather['weather']


def read_value(cat, subcat):
    global weather
    if weather is None:
        return None
    try:
        return weather[cat][subcat]
    except KeyError:
        return None


conditions = {200: 'thunderstorm with light rain', 201: 'thunderstorm with rain', 202: 'thunderstorm with heavy rain', 210: 'light thunderstorm', 211: 'thunderstorm', 212: 'heavy thunderstorm',
              221: 'ragged thunderstorm', 230: 'thunderstorm with light drizzle', 231: 'thunderstorm with drizzle', 232: 'thunderstorm with heavy drizzle',
              300: 'light intensity drizzle', 301: 'drizzle', 302: 'heavy intensity drizzle', 310: 'light intensity drizzle rain', 311: 'drizzle rain', 312: 'heavy intensity drizzle rain',
              313: 'shower rain and drizzle', 314: 'heavy shower rain and drizzle', 321: 'shower drizzle', 500: 'light rain', 501: 'moderate rain', 502: 'heavy intensity rain',
              503: 'very heavy rain', 504: 'extreme rain', 511: 'freezing rain', 520: 'light intensity shower rain', 521: 'shower rain', 522: 'heavy intensity shower rain',
              531: 'ragged shower rain', 600: 'light snow', 601: 'snow', 602: 'heavy snow', 611: 'sleet', 612: 'light shower sleet', 613: 'shower sleet', 615: 'light rain and snow',
              616: 'rain and snow', 620: 'light shower snow', }


# https://openweathermap.org/weather-conditions
# Group 2xx: Thunderstorm
# ID	    Main	        Description
# 200	    Thunderstorm	thunderstorm with light rain
# 201	    Thunderstorm	thunderstorm with rain
# 202	    Thunderstorm	thunderstorm with heavy rain
# 210	    Thunderstorm	light thunderstorm
# 211	    Thunderstorm	thunderstorm
# 212	    Thunderstorm	heavy thunderstorm
# 221	    Thunderstorm	ragged thunderstorm
# 230	    Thunderstorm	thunderstorm with light drizzle
# 231	    Thunderstorm	thunderstorm with drizzle
# 232	    Thunderstorm	thunderstorm with heavy drizzle
# Group 3xx: Drizzle
# ID	    Main	        Description
# 300	    Drizzle	        light intensity drizzle
# 301	    Drizzle	        drizzle
# 302	    Drizzle	        heavy intensity drizzle
# 310	    Drizzle	        light intensity drizzle rain
# 311	    Drizzle	        drizzle rain
# 312	    Drizzle	        heavy intensity drizzle rain
# 313	    Drizzle	        shower rain and drizzle
# 314	    Drizzle	        heavy shower rain and drizzle
# 321	    Drizzle	        shower drizzle
# Group 5xx: Rain
# ID	    Main	        Description
# 500	    Rain	        light rain
# 501	    Rain	        moderate rain
# 502	    Rain	        heavy intensity rain
# 503	    Rain	        very heavy rain
# 504	    Rain	        extreme rain
# 511	    Rain	        freezing rain
# 520	    Rain	        light intensity shower rain
# 521	    Rain	        shower rain
# 522	    Rain	        heavy intensity shower rain
# 531	    Rain	        ragged shower rain
# Group 6xx: Snow
# ID	    Main	        Description
# 600	    Snow	        light snow
# 601	    Snow	        Snow
# 602	    Snow	        Heavy snow
# 611	    Snow	        Sleet
# 612	    Snow	        Light shower sleet
# 613	    Snow	        Shower sleet
# 615	    Snow	        Light rain and snow
# 616	    Snow	        Rain and snow
# 620	    Snow	        Light shower snow
# 621	    Snow	        Shower snow
# 622	    Snow	        Heavy shower snow
# Group 7xx: Atmosphere
# ID	    Main	        Description
# 701	    Mist	        mist
# 711	    Smoke	        Smoke
# 721	    Haze	        Haze
# 731	    Dust	        sand/ dust whirls
# 741	    Fog	            fog
# 751	    Sand	        sand
# 761	    Dust	        dust
# 762	    Ash	            volcanic ash
# 771	    Squall	        squalls
# 781	    Tornado	        tornado
# Group 800: Clear
# ID	    Main	        Description
# 800	    Clear   	    clear sky
# Group 80x: Clouds
# ID	    Main	        Description
# 801	    Clouds	        few clouds: 11-25%
# 802	    Clouds	        scattered clouds: 25-50%
# 803	    Clouds	        broken clouds: 51-84%
# 804	    Clouds	        overcast clouds: 85-100%
