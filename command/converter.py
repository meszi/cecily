import logging
from core.util import read_webpage_source


def convert(amount, from_, to):
    """ Converts between currencies using google. """
    log = logging.getLogger('Cecily.Converter')
    url = "https://www.google.hu/search?q={}+{}+in+{}&hl=en&num=1&start=0".format(amount, from_, to)
    s = read_webpage_source(url)

    log.debug("Convert got response: %s", s)

    start = s.find('<table class="std _tLi">')
    end = s.find('</table>', start)
    result_table = s[start:end]

    value_start = result_table.find('<b>')
    value_end = result_table.find('</b>', value_start)
    result = result_table[value_start+len('<b>'):value_end]
    return result
