import logging
import sqlite3


class DB:

    instance = None

    def __new__(cls, file):
        if DB.instance:
            raise ValueError('Database instance already created, use get_instance instead')
        DB.instance = DB.__DB(file)
        return DB.instance

    @staticmethod
    def get_instance():
        if not DB.instance:
            raise ValueError('Database instance not yet created')
        return DB.instance

    class __DB:

        def __init__(self, file):
            self.log = logging.getLogger('Cecily.DB')
            self.log.debug('Connecting to database %s', file)
            self.conn = sqlite3.connect(file)
            self.cursor = self.conn.cursor()

        def __del__(self):
            self.close()

        def execute(self, stmt, params=None):
            self.log.debug('Executing: %s with params: %s', stmt, params)
            if not params:
                return self.cursor.execute(stmt)
            else:
                return self.cursor.execute(stmt, params)

        def commit(self):
            self.log.debug('Commiting')
            self.conn.commit()

        def close(self):
            self.log.debug('Closing database connection')
            self.conn.close()

