from storage.db import DB


def save_activity(userid, username, activity):
    db = DB.get_instance()
    db.execute("""INSERT INTO channelActivity (time, userId, userName, activity) VALUES (datetime('now', 'localtime'), ?, ?, ?);""", [userid, username, activity])
    db.commit()


def activity_history(count):
    db = DB.get_instance()
    cursor = db.execute("""SELECT * FROM channelActivity ORDER BY time DESC LIMIT ?""", str(count))
    result = cursor.fetchall()
    result.reverse()
    s = '\n'
    for activity in result:
        s += escape_timestamp_emoji(activity[0]) + ' ' + (activity[2] if activity[2] is not None else 'someone') + ' ' + activity[3] + '\n'

    return s


def escape_timestamp_emoji(ts):
    """ 2019-03-14 18:35:02 , where the :0 part becomes an emoji, so put spaces around colons """
    return ts.replace(':', ' : ')


def free_game_found(title, store):
    db = DB.get_instance()
    db.execute("""INSERT INTO freeGames VALUES (datetime('now', 'localtime'), ?, ?);""", [title, store])
    db.commit()

