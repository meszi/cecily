#!/usr/bin/env bash

PIDFILE=/proj/cecily/cecily.pid

case $1 in
   start)	  
	  cd /proj/cecily/
      /usr/bin/python3 cecily.py &
	  echo $! > ${PIDFILE};	  
   ;;
   stop)
      kill -9 `cat ${PIDFILE}`;
	  rm ${PIDFILE};
   ;;
esac
exit 0
