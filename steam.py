import json
import logging
from core.util import read_webpage_source, extract_url
from core.chat import Chat

API_URL = 'https://store.steampowered.com/api/appdetails?appids='
log = logging.getLogger("Cecily.SteamHelper")


def describe_link(payload):
    appid = extract_app_id(extract_url(payload))
    try:
        src = read_webpage_source(API_URL + appid)
    except RuntimeError:
        log.error('Cannot open URL: %s', API_URL + appid)
        Chat.send_to_channel('Well... I cannot reach the steam API right now.')
        return
    root = json.loads(src)
    title = root[appid]['data']['name']
    price = 'Free' if root[appid]['data']['is_free'] else get_price(root[appid]['data']['price_overview'])
    genres = ''
    for genre in root[appid]['data']['genres']:
        genres += genre['description'] + '|'
    genres = genres[:-1]
    Chat.send_to_channel(Chat.with_format('[Steam] {} - {}\n{}'.format(title, price, genres), 'b'))


def get_price(json):
    buffer = json['final_formatted']
    if '-' in buffer:
        buffer = buffer.replace('-', '')
        buffer = buffer.replace(',', '')
    if json['discount_percent'] > 0:
        buffer += ' (-{}%)'.format(json['discount_percent'])
    return buffer


def extract_app_id(url):
    prefix = 'store.steampowered.com/app/'
    start = url.find(prefix)+len(prefix)
    return url[start:url.find('/', start)]
