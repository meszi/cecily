import re
import logging
from core import util, handlers
from core.chat import Chat

log = logging.getLogger('Cecily.EpicGamesStore')


class EpicStoreHandler(handlers.EventHandler):

    def handle(self, event):
        super().handle(event)
        games = get_free_games()
        if games is None:
            log.info('Didn\'t find any free games.')
            return
        # check in db to see if these are already found

        # if yes, make sure channel description is up to date with these games

        # if not, send a message about the new games, and refresh the channel description



def get_free_games():
    log.info('Querying the epic games store for free games')
    try:
        src = util.read_rendered_webpage_source('https://www.epicgames.com/store/en-US/free-games')
    except RuntimeError:
        log.error('Error opening the epic games store')
        return None
    start = src.find('FreeGamesCollection-section')
    end = src.find('FreeGamesCollection-section', start+len('FreeGamesCollection-section'))
    src = src[start:end]
    games = []

    while src.find('OfferTitleInfo-title') != -1:
        title_start = src.find('OfferTitleInfo-title')
        title_start = src.find('>', title_start)+1
        title_end = src.find('</span>', title_start)
        title = src[title_start:title_end]

        schedule_start = src.find('<span>', title_end)
        schedule_end = src.find('</span>', schedule_start)
        schedule = re.sub('<[^>]*>', '', src[schedule_start:schedule_end])
        games.append((title, schedule))

    return games
