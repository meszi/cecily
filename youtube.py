import logging
import math
from core.util import read_webpage_source, extract_url, is_int_or_float
from core.chat import Chat

log = logging.getLogger("Cecily.YoutubeHelper")


def describe_link(payload):
    url = extract_url(payload)
    log.debug('Collecting info from youtube link: %s', url)
    source = read_webpage_source(url)
    title = extract_title(source)
    length = to_min_sec(extract_length(source))
    Chat.send_to_channel(Chat.with_format('[YouTube] {} [{}]'.format(title, length), 'b'))


def extract_title(source):
    start = source.find('<meta property="og:title" content=')+35
    end = source.find('">', start)
    return source[start:end]


def extract_length(source):
    """ first match of "lengthSeconds":"1430", """
    start = source.find('lengthSeconds')+13
    end = source.find(',', start)
    # \":\"103\",
    nums = []
    print(source[start:end])
    for ch in source[start:end]:
        if is_int_or_float(ch):
            nums += ch
            print(nums)
    return ''.join(nums)


def to_min_sec(seconds):
    try:
        sec = int(seconds)
        s = str((sec % 60))
        s_prefixed = s if len(s) == 2 else '0' + s
        return str(math.floor(sec / 60)) + ' : ' + s_prefixed
    except ValueError:
        return "length unknown"

