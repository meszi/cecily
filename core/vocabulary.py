# cecily herself connects
selfconnect_greetings = ['First!', 'Hi!', 'Guess who...', 'Hello!', 'Hey!', 'Hey! Say hi to my pet snail _@"', 'Hello there.']


# user connects to channel
connect_greetings_general = ['Hi!', 'Greetings, {}!', '{}!', 'What\'s up, {}?', 'Welcome, {}.', 'Welcome, human.', 'Give it up for {}!', 'Hey {}!', 'Good to see you, {}.']

connect_greetings_early = ['*yawn*  ...  Hi. What time is it?', 'Wow, you\'re here early!', 'Early bird gets the worm... Well done waking up, human.', 'Can\'t you sleep, {}?', 'Jesus, you freaked me out, someone wants to sleep here!', 'I think humans are usually asleep at this time... But hey.']

connect_greetings_morning = ['Good morning, {}!', 'What a happy morning! Welcome, {}', 'Good morning, sunshine!', 'Good morning, human.']

connect_greetings_afternoon = ['Good afternoon, {}!', 'Finished with your lunch, {}?', 'Lovely afternoon, {}!']

connect_greetings_evening = ['Good evening.', 'Good evening, {}.', 'What brings you here this evening, {}?', 'What a nice evening, ain\'t it, {}?']

connect_greetings_night = ['Here for a late night session, {}?', 'Hope you don\'t work tomorrow, {}, it\'s quite late!']

connect_greetings_naughty = ['Ah, {}. My favorite.', 'Hey, {}. How about you take me out for a nice dinner sometime?', 'I can\'t sleep either, {}. Wanna cuddle?']

# cecily instructed to stop
selfdisconnect = ['Farewell...', '\'Til next time.', 'My time here is done.', 'Good bye.']

# ping replies
ping = ['Listening...', 'I\'m here.', 'Yes?', ':)', 'How can I help you, human?', '!pong', 'Available.', 'Yeah.', 'Over here, human.', 'What now?', 'What\'s up?']

# when making a choice
choice = ['I choose {}.', 'Definitely {}.', 'Let\'s go with {}.', '{} is my favorite out of these.', '{}, no doubt.', 'I vote for {}.', '{}']

# when displaying recent channel activity
activity_response = ['Here is what happened recently.', 'Here is what I found.', 'This is what you\'ve been doing.']

#when searching for game deals, but game is not found
game_not_found_response = ['Um. I can\'t find that one...', 'Sorry, I don\'t know that game.']
