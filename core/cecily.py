import sys
import argparse
import configparser
import logging
import time
from core.eventbroker import EventBroker
from storage.db import DB
from core import handlers, comms
from core.event import EventType, Event
from channel_manager import ChannelInfoHandler
import epic_store, ubisoft_store

# parse config
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-config', default='prod.ini')
arg_parser.add_argument('-logtostdout', action='store_true')
args = arg_parser.parse_args()
config_parser = configparser.ConfigParser()
config_parser.read(args.config)
config = config_parser['CONFIG']


def main():
    # initialize logging
    log = logging.getLogger('Cecily')
    logging.basicConfig(filename=config['log_dir'], filemode='a', level=config['log_level'], format='%(asctime)s %(name)s %(levelname)s %(message)s')
    if args.logtostdout:
        import sys
        stdout = logging.StreamHandler(sys.stdout)
        formatter = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s')
        stdout.setFormatter(formatter)
        log.addHandler(stdout)
    log.info('*** CECILY STARTUP ***')
    log.info('Logging initialized...')

    # setup static event subscribers
    e_broker = EventBroker()
    connect_handler = handlers.ConnectHandler()
    e_broker.subscribe(EventType.USER_JOINED, connect_handler)
    msg_handler = handlers.MessageHandler()
    e_broker.subscribe(EventType.TEXT_MESSAGE, msg_handler)
    init_handler = handlers.InitHandler()
    e_broker.subscribe(EventType.INIT_EVENT, init_handler)
    on_registered_handler = handlers.OnRegisteredHandler()
    e_broker.subscribe(EventType.ON_REGISTERED, on_registered_handler)
    read_handler = handlers.ReadHandler()
    e_broker.subscribe(EventType.TELNET_READ, read_handler)
    disconnect_handler = handlers.DisconnectHandler()
    e_broker.subscribe(EventType.USER_LEFT, disconnect_handler)
    channel_handler = ChannelInfoHandler()
    e_broker.subscribe(EventType.CHANNEL_INFO, channel_handler)

    # setup periodic event subscribers
    virus_handler = handlers.VirusHandler()
    e_broker.subscribe(EventType.VIRUS, virus_handler)
    e_broker.periodic_event(EventType.VIRUS, 120)
    epic_handler = epic_store.EpicStoreHandler()
    e_broker.subscribe(EventType.EPIC_GAME_STORE, epic_handler)
    e_broker.periodic_event(EventType.EPIC_GAME_STORE, 3600)
    ubisoft_handler = ubisoft_store.UbisoftHandler()
    e_broker.subscribe(EventType.UBISOFT_STORE, ubisoft_handler)
    e_broker.periodic_event(EventType.UBISOFT_STORE, 3600)

    log.info('Event handlers initialized...')

    db = DB(config['db'])
    log.info('Database loaded...')

    # TODO mood based on something - news? market index?
    # TODO handle other events
    # TODO deal with any channel structure?
    # TODO events - christmas
    # TODO more exception handling and logging
    # TODO unit tests - errors might occur based on part of day, etc
    # TODO responses sometimes hardcoded instead of vocabulary choice

    # events firing every half sec
    TICK_RESOLUTION = 0.5
    comms.init(config)
    e_broker.new_event(Event(EventType.INIT_EVENT, config))
    log.info("Connecting...")

    while True:
        try:
            # blocks for 200ms max
            comms.receive()
            start = time.time()
            e_broker.tick()
            end = time.time()
            elapsed = end - start
            if elapsed < TICK_RESOLUTION:
                time.sleep(TICK_RESOLUTION-elapsed)

        except EOFError as e:
            log.error('Connection interrupted: %s', str(e))
            shutdown()
        except OSError as e:
            log.error('Error establishing connection: %s', str(e))
            shutdown()
        except:
            e = sys.exc_info()
            log.error('Top level error: %s', e)
            shutdown()


def shutdown():
    comms.close()
    sys.exit(1)


if __name__ == '__main__':
    main()
