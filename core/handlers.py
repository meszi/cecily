import logging
from core.util import get_params
from core.partofday import PartOfDay
from core.chat import Chat
from core import vocabulary
from command import commands
from core.comms import send
from core.event import Event, EventType
from core.eventbroker import EventBroker
from storage import db_agent as DBA
from core.util import online_users


class EventHandler:

    log = logging.getLogger('Cecily.EventHandler')

    def handle(self, event):
        self.log.info('Event received: %s', event)


##########################################################
e_broker = EventBroker()


class InitHandler(EventHandler):

    def handle(self, event):
        super().handle(event)
        config = event.payload
        send('login ' + config['server_user'] + ' ' + config['server_password'])
        self.log.debug('Logging in with %s as user and %s as password', config['server_user'], config['server_password'])
        send('use port=' + config['query_port'])
        self.log.debug('Using port %s', config['query_port'])
        send('clientupdate client_nickname=Cecily')
        self.log.debug('Using name Cecily')
        # TODO don't hardcode channel id
        send('servernotifyregister event=channel id=1')
        send('servernotifyregister event=textchannel')
        send('servernotifyregister event=textprivate')
        e_broker.new_event(Event(EventType.ON_REGISTERED, None))


class OnRegisteredHandler(EventHandler):
    def handle(self, event):
        super().handle(event)
        # TODO can be enabled once there are no transient disconnects
        #Chat.send_to_channel(Chat.with_format('*Cecily connects*', 'b+color=green'))
        #greetings = vocabulary.selfconnect_greetings
        #Chat.send_to_channel(random.choice(greetings))


class ReadHandler(EventHandler):
    def handle(self, event):
        super().handle(event)
        incoming = event.payload
        # a chat message arrived
        if incoming.startswith('notifytextmessage'):
            e_broker.new_event(Event(EventType.TEXT_MESSAGE, incoming))
        # user joined channel
        elif incoming.startswith('notifycliententerview'):
            e_broker.new_event(Event(EventType.USER_JOINED, incoming))
        # user left channel
        elif incoming.startswith('notifyclientleftview'):
            e_broker.new_event(Event(EventType.USER_LEFT, incoming))
        # channel info response
        elif incoming.startswith('pid=0 channel_name'):
            e_broker.new_event(Event(EventType.CHANNEL_INFO, incoming))


class ConnectHandler(EventHandler):
    def handle(self, event):
        super().handle(event)
        params = get_params(event.payload)
        name = params['client_nickname']
        greetings = {PartOfDay.EARLY: vocabulary.connect_greetings_general + vocabulary.connect_greetings_early,
                     PartOfDay.MORNING: vocabulary.connect_greetings_general + vocabulary.connect_greetings_morning,
                     PartOfDay.AFTERNOON: vocabulary.connect_greetings_general + vocabulary.connect_greetings_afternoon,
                     PartOfDay.EVENING: vocabulary.connect_greetings_general + vocabulary.connect_greetings_evening,
                     PartOfDay.NIGHT: vocabulary.connect_greetings_general + vocabulary.connect_greetings_night,
                     PartOfDay.NAUGHTY: vocabulary.connect_greetings_general + vocabulary.connect_greetings_naughty}
        Chat.send_to_channel(PartOfDay.random_pod_choice(greetings).format(name))
        DBA.save_activity(params['clid'], name, 'joined')
        online_users[params['clid']] = name


class DisconnectHandler(EventHandler):
    def handle(self, event):
        super().handle(event)
        params = get_params(event.payload)
        clid = params['clid']
        if clid in online_users:
            DBA.save_activity(clid, online_users[clid], 'left')
            online_users.pop(clid)
        else:
            DBA.save_activity(clid, None, 'left')


class MessageHandler(EventHandler):

    log = logging.getLogger('Cecily.MessageHandler')

    def handle(self, event):
        super().handle(event)
        params = get_params(event.payload)
        payload = params['msg']
        self.log.debug('Payload: %s', payload)
        invokername = params['invokername']
        invokerid = params['invokerid']
        targetmode = params['targetmode']
        self.log.info('got: %s from %s with id %s', payload, invokername, invokerid)
        if payload.startswith('!') and payload.split('\s')[0] in commands.COMMANDS:
            self.log.info('reacting to command: %s', payload)
            commands.invoke(commands.Command(payload, invokername, invokerid, targetmode))
        elif payload.startswith('[URL]') and ('youtube.com\/watch' in payload or 'youtu.be\/' in payload):
            import youtube
            youtube.describe_link(payload)
        elif payload.startswith('[URL]') and ('store.steampowered.com' in payload):
            import steam
            steam.describe_link(payload)


class VirusHandler(EventHandler):
    from datetime import datetime
    last_timestamp = datetime.now()

    def handle(self, event):
        super().handle(event)
        import datetime
        from command import virus
        msg = virus.parse_stats()
        if msg is None:
            return
        #2020.03.16. 18:31
        datestr = msg[msg.find('As of ')+6:].split('.')
        current_timestamp = datetime.datetime(int(datestr[0]), int(datestr[1]), int(datestr[2]), int(datestr[-1].split(':')[0]), int(datestr[-1].split(':')[1]))
        if current_timestamp > VirusHandler.last_timestamp:
            msg = '\nCOVID-19 stats update' + msg
            Chat.send_to_channel(msg)
            VirusHandler.last_timestamp = current_timestamp



