import sys
import logging
import urllib.request
import html
from selenium import webdriver

log = logging.getLogger("Cecily.Util")


def is_int_or_float(s):
    try:
        int(s)
        return True
    except ValueError:
        try:
            float(s)
            return True
        except ValueError:
            return False


def extract_number(s):
    num = ''
    for ch in s:
        if is_int_or_float(ch):
            num += ch
    return int(num) if num != '' else None


INT_REGEX = '[0-9]+'
STR_REGEX = '.*'


def get_params(msg):
    """ Returns parameters of a query result (key-value pairs) """
    parts = msg.split()
    params = {}
    for part in parts:
        if '=' in part:
            key, value = part.split('=', 1)
            params[key] = value
    return params


def read_webpage_source(url):
    log.info('Opening URL: %s', url)
    user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
    headers = {'User-Agent': user_agent}
    try:
        request = urllib.request.Request(url, None, headers)
        response = urllib.request.urlopen(request, timeout=10)
        data = response.read()

        return html.unescape(data.decode('utf-8'))
    except:
        e = sys.exc_info()
        log.error('Error reading webpage source: %s', e)
        raise RuntimeError('Cannot read webpage source')


def read_rendered_webpage_source(url, wait_time=3):
    log.info('Opening URL: %s', url)
    try:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        driver = webdriver.Chrome(chrome_options=chrome_options)
        driver.get(url)
        driver.implicitly_wait(wait_time)
        source = driver.page_source
        driver.quit()
        return source
    except:
        e = sys.exc_info()
        log.error('Error rendering webpage: %s', e)
        raise RuntimeError('Cannot render webpage')


def extract_url(payload):
    """ [URL]https:\/\/youtu.be\/k0gvAyCubGQ[\/URL] """
    start = payload.find('[URL]')+5
    end = payload.find('[\/URL]')
    return payload[start:end].replace('\\', '')

# mapping between client ids and usernamaes
online_users = {}

# TODO not yet set or used anywhere
# id of the current channel
channel_id = None
