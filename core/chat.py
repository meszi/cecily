from core.comms import send


class Chat:

    DEFAULT_COLOR = '#555555'

    @staticmethod
    def prepare_msg(msg):
        """ Escape characters and apply formatting """
        # TODO escaping
        msg = msg.replace(' ', '\s')
        msg = msg.replace('\n', '\\n')
        msg = msg.replace('|', '\p')
        if 'color=' not in msg and Chat.DEFAULT_COLOR != '#000000':
            msg = Chat.with_format(msg, 'color='+Chat.DEFAULT_COLOR)
        return msg

    @staticmethod
    def send_to_channel(msg):
        """ Sends a message to the current channel. """
        msg = Chat.prepare_msg(msg)
        send('sendtextmessage targetmode=2 target=1 msg=' + msg)

    @staticmethod
    def send_to_client(msg, uid):
        """ Sends a message to a client. """
        # TODO how to target clients
        msg = Chat.prepare_msg(msg)
        send('sendtextmessage targetmode=1 target=' + uid + ' msg=' + msg)

    @staticmethod
    def send_to_target(msg, uid, target_mode):
        """ Sends a message to a specified target. """
        msg = Chat.prepare_msg(msg)
        send('sendtextmessage targetmode=' + target_mode + ' target=1 msg=' + msg)

    @staticmethod
    def with_format(msg, formatstr):
        """ Encloses the text in specified formatting tags
            b -> bold, i -> italic, u -> underlined
            color=#hex or color=red -> colored text
            url=... -> hyperlink pointing to ...
            Specify more than one format string delimited by +
             e.g. b+i will yield bold and italic
             b+i+color=blue will yield bold, italic and blue color
        """
        # if more than one format string specified, recursively call until all is dealt with
        if '+' in formatstr:
            fmt_strings = formatstr.split('+')
            for f in fmt_strings:
                msg = Chat.with_format(msg, f)
            return msg

        if 'color' in formatstr:
            return '[{0}]{1}[/color]'.format(formatstr, msg)
        elif 'url' in formatstr:
            return '[{0}]{1}[/url]'.format(formatstr, msg)
        else:
            return '[{0}]{1}[/{0}]'.format(formatstr, msg)