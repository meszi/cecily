import telnetlib
import logging
import time
import threading
from core.event import EventType, Event
from core.eventbroker import EventBroker

log = logging.getLogger('Cecily.Comms')

tn = None

# keep this low for the event loop to be responsive
BLOCK_MAX = 0.2


def init(config):
    global tn, heartbeat
    tn = telnetlib.Telnet(config['server_host'], config['server_port'])
    heartbeat = Heartbeat(int(config['timeout']))


class Heartbeat:

    def __init__(self, timeout):
        log.debug('Heartbeat starting with %s timeout', timeout)
        self.timeout = timeout
        self.counter = timeout
        self.thread = threading.Thread(target=self.start, name='heartbeat')
        self.thread.setDaemon(True)
        self.lock = threading.Lock()
        self.thread.start()

    def start(self):
        self.reset()
        while True:
            while self.not_timed_out():
                time.sleep(1)
                self.counter -= 1
            self.heartbeat()

    def not_timed_out(self):
        self.lock.acquire()
        r = self.counter > 0
        self.lock.release()
        return r

    def reset(self):
        self.lock.acquire()
        self.counter = self.timeout
        self.lock.release()

    def heartbeat(self):
        send('version')
        log.debug('Heartbeat prompt sent...')
        self.reset()


heartbeat = None
e_broker = EventBroker()


def send(msg):
    tn.write(encode(msg))
    heartbeat.reset()


def receive():
    # every message is terminated with a newline
    # block for N seconds max
    msg = decode(tn.read_until(b'\n', BLOCK_MAX))
    if msg is not None:
        e_broker.new_event(Event(EventType.TELNET_READ, msg))


def encode(msg):
    return (msg + '\n').encode('utf-8')


def decode(msg):
    if len(msg) > 0:
        return msg.decode().strip()


def close():
    tn.close()


