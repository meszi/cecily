class EventType:

    HEARTBEAT_RESPONSE = 0
    USER_JOINED = 1
    TEXT_MESSAGE = 2
    INPUT_REQUEST = 3
    OUTPUT_REQUEST = 4
    INIT_EVENT = 5
    ON_REGISTERED = 6
    TELNET_READ = 7
    USER_LEFT = 8
    VIRUS = 9
    CHANNEL_INFO = 10
    EPIC_GAME_STORE = 11
    UBISOFT_STORE = 12


class Event:

    def __init__(self, event_type, payload):
        self.type = event_type
        self.payload = payload

    def __str__(self):
        return 'Event: type: {}, payload: {}'.format(self.type, self.payload)
