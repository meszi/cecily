import logging
import multiprocessing, queue
import threading
import datetime
from core.event import Event


class EventBroker:

    instance = None

    def __new__(cls):
        if not EventBroker.instance:
            EventBroker.instance = EventBroker.__EventBroker()
        return EventBroker.instance

    class __EventBroker:

        log = logging.getLogger('Cecily.EventBroker')
        # map an event type to a list of handlers (that subscribe to it)
        subscriptions = {}
        subscription_lock = threading.Lock()
        event_queue = multiprocessing.Queue()
        # map an event type to be fired every <period> seconds
        periodic_events = {}
        # contains upcoming periodic event types with next timestamp to fire at
        periodic_upcoming = {}

        def subscribe(self, event_type, handler):
            """ An event handler subscribes to an event type """
            self.subscription_lock.acquire()
            if event_type in self.subscriptions:
                self.subscriptions[event_type].append(handler)
            else:
                self.subscriptions[event_type] = [handler]
            self.subscription_lock.release()

        def unsubscribe(self, event_type, handler):
            """ An event handler unsubscribes from an event type """
            self.subscription_lock.acquire()
            if event_type in self.subscriptions and handler in self.subscriptions[event_type]:
                self.subscriptions[event_type].remove(handler)
            self.subscription_lock.release()

        def new_event(self, event):
            """ Emits an event """
            self.event_queue.put_nowait(event)

        def periodic_event(self, event_type, period):
            """ Registers an event type to be fired every <period> seconds """
            self.periodic_events[event_type] = period
            self.periodic_upcoming[event_type] = datetime.datetime.now() + datetime.timedelta(seconds=period)

        # TODO cancel periodic

        def tick(self):
            """ Calls event handlers based on subscriptions, fires periodic events when due """
            self.check_periodic()
            e = self.next_event()
            while e is not None:
                for handler in self.subscriptions[e.type]:
                    handler.handle(e)
                e = self.next_event()

        def next_event(self):
            """ Gets the next event out of the queue without blocking """
            try:
                e = self.event_queue.get_nowait()
                return e
            except queue.Empty:
                return None

        def check_periodic(self):
            """  Checks if any periodic event needs to be fired """
            now = datetime.datetime.now()
            for e_type in self.periodic_upcoming:
                if self.periodic_upcoming[e_type] <= now:
                    self.new_event(Event(e_type, None))
                    self.periodic_upcoming[e_type] = now + datetime.timedelta(seconds=self.periodic_events[e_type])
