from datetime import datetime
import random
import logging


class PartOfDay:

    log = logging.getLogger('Cecily.PartOfDay')

    EARLY = 0 # 4-9
    MORNING = 1 # 9-12
    AFTERNOON = 2 # 12-19
    EVENING = 3 # 19-22
    NIGHT = 4 # 22-24
    NAUGHTY = 5 # 0-4

    @staticmethod
    def now():
        now = datetime.now()
        h = now.hour
        if 4 <= h < 9:
            return PartOfDay.EARLY
        elif 9 <= h < 12:
            return PartOfDay.MORNING
        elif 12 <= h < 19:
            return PartOfDay.AFTERNOON
        elif 19 <= h < 22:
            return PartOfDay.EVENING
        elif 22 <= h:
            return PartOfDay.NIGHT
        elif 0 <= h < 4:
            return PartOfDay.NAUGHTY

    @staticmethod
    def to_str(pod):
        if pod == PartOfDay.EARLY:
            return 'EARLY'
        elif pod == PartOfDay.MORNING:
            return 'MORNING'
        elif pod == PartOfDay.AFTERNOON:
            return 'AFTERNOON'
        elif pod == PartOfDay.EVENING:
            return 'EVENING'
        elif pod == PartOfDay.NIGHT:
            return 'NIGHT'
        elif pod == PartOfDay.NAUGHTY:
            return 'NAUGHTY'

    @staticmethod
    def random_pod_choice(choices):
        """ Chooses list value from {part of day -> list} dict, according to current part of day
        Choices should be dictionary with lists for each part of day """
        try:
            pod = PartOfDay.now()
            PartOfDay.log.info('part of day is %s', PartOfDay.to_str(pod))
            return random.choice(choices[pod])
        except KeyError:
            PartOfDay.log.error('Dictionary does not contain choices for this part of day!')