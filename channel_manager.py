from core.comms import send
from core.handlers import EventHandler

current_description = None


def get_description():
    # TODO don't hardcode channel id
    current_description = send('channelinfo cid=1')


class ChannelInfoHandler(EventHandler):
    def handle(self, event):
        super().handle(event)
        description_start = event.payload.find('channel_description=') + 20
        description_end = event.payload.find(' ', description_start)
        global current_description
        current_description = event.payload[description_start:description_end]


def update_channel_description(description):
    # TODO don't hardcode channel id
    log.info('Updating channel description')
    description = 'Free on the epic games store: \n'
    for game in games:
        description += game[0] + ' (' + game[1] + ')\n'
    description += '[url]https://www.epicgames.com/store/en-US/free-games[/url]\n'
    send('channeledit cid=1 channel_description=' + Chat.prepare_msg(description))