import unittest
from storage.db import DB


class DBTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(DBTest, self).__init__(*args, **kwargs)
        self.db = DB('test.sqlite')

    def test_create(self):
        r = self.db.execute("""CREATE TABLE IF NOT EXISTS channelActivity (
                    time     TEXT,
                    userId   INTEGER,
                    userName TEXT,
                    activity TEXT
                    );""")

        # -1 returned since nothing to count
        self.assertEqual(-1, r.rowcount)
        self.db.commit()

    def test_insert(self):
        r = self.db.execute("""INSERT INTO channelActivity (activity, userName, userId, time)
                            VALUES ('connected', 'TestUser', 1, datetime('now')),
                                   ('disconnected', 'TestUser', 2, datetime('now'));""")
        # two rows inserted
        self.assertEqual(2, r.rowcount)
        self.db.commit()

    def test_read(self):
        r = self.db.execute("""SELECT * FROM channelActivity""")
        rows = r.fetchall()
        self.assertEqual('connected', rows[0][3])
        self.assertEqual('disconnected', rows[1][3])

    # TODO
    def test_update(self):
        pass

    # TODO
    def test_delete(self):
        pass




